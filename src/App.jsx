import './App.css'
import Map from './Components/Map'

function App() {
  

  return (
    <div className='m-0  flex flex-col items-center justify-center mt-14 '>
        <Map />
        <div className=' flex justify-evenly items-center mt-6 p-2 gap-5 md:gap-1 w-4/5  '>
        <button className='p-2 text-center  font-bold md:text-xl text-lg bg-yellow-600 rounded-lg '
        onClick=""
        >
          validate
        </button>
        <button className=' p-2 text-center  font-bold md:text-xl text-lg bg-red-400 rounded-lg  '>
          Issued
        </button>
        <button className=' p-2 text-center  font-bold md:text-xl text-lg bg-blue-500 rounded-lg'>
          Fraud Match
        </button>

        </div>
      </div>
        
   
  )
}

export default App
