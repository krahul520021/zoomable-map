import React, { useEffect, useRef } from 'react';
import 'ol/ol.css';
import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import Circle from 'ol/geom/Circle';
import { Style, Fill, Stroke } from 'ol/style';
import { fromLonLat } from 'ol/proj';

const OpenLayersMap = () => {
  const mapElement = useRef(null);

  useEffect(() => {
    const center = fromLonLat([78.9629, 20.5937]); // Longitude, Latitude of the center of India
    const radius = 500000; // Radius in meters

    const circularFeature = new Feature(new Circle(center, radius));

    const vectorSource = new VectorSource({
      features: [circularFeature],
    });

    const vectorLayer = new VectorLayer({
      source: vectorSource,
      style: new Style({
        fill: new Fill({
          color: 'rgba(226, 131, 48, 0.6)', 
        }),
        stroke: new Stroke({
          color: '#ff6600',
          width: 2,
        }),
      }),
    });

    const map = new Map({
      target: mapElement.current,
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
        vectorLayer,
      ],
      view: new View({
        center: center,
        zoom: 3,
      }),
    });
    return () => {
      map.setTarget(null);
    };
  }, []);

  return (
    <div style={{ width: '1382px', height: '600px', border: '2px solid white', overflow: 'hidden' }}>
      <div ref={mapElement} style={{ width: '100%', height: '100%' }}></div>
    </div>
  );
};

export default OpenLayersMap;
